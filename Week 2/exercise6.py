import textwrap


string = input("Input string: ")
width = int(input("Input width: "))
lines = textwrap.wrap(string, width)
print("This is the paragraph of width {}".format(width))
print("{}".format('\n'.join(lines)))
